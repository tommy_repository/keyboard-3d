package com.example.tomis.keyboard3d;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Toast;

import com.example.tomis.keyboard3d.classes.KeyboardSensor;
import com.example.tomis.keyboard3d.classes.KeyboardService;
import com.example.tomis.keyboard3d.classes.Preferences;


/**
 * Created by tomis on 2/7/2016.
 */
public class SimpleIME extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener, SensorEventListener {

    private KeyboardView kv;
    private Keyboard qwerty_special_chars,aeiou,qwerty,qwerty_1,qwerty_2,qwerty_3,qwerty_4,qwerty_5;
    private Keyboard nums;
    private Keyboard activeKeyboard;
    protected static final String TAG = "FrequencyAdapter";

    private KeyboardService keyboardService;

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private Sensor magnetometer;

    private SensorCalculations Calculations;
    private int selectedCode = 0;

    private boolean caps = false;
    private int codeFromSensor = 0;
    private String SensorCode = "";

    private Preferences preferences;

    private int[] ifRedirectHome = {
//            32,33,35,36,37,38,39,40,41,42,43,44,45,46,61,63,91,93,123,125
            500
    };
    private String keyboardType;
    private KeyboardSensor sensorCalculations;
    private float[] mGravity;
    private float[] mGeomagnetic;
    private boolean shown;


    @Override
    public void onCreate() {
        super.onCreate();
//        Toast.makeText(this, "keyboard visible", Toast.LENGTH_SHORT).show();

        initPreferences();
//        initSensor();
        createKeyboard();
        setKeyboardType(preferences.getValue("keyboard_type"));
    }

    private void createKeyboard() {
        final SimpleIME self = this;
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        params.gravity = Gravity.BOTTOM;
        getWindow().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        keyboardService =  new KeyboardService();
        keyboardService.setPreferences(preferences);
        keyboardService.setContext(getLayoutInflater().getContext());

        keyboardService.setOnClick(new MyInterface() {
            @Override
            public void composite(int code) {
                self.compositeText(code);
            }

            @Override
            public void write(int code) {
//                int code = (int) character;
                self.write(code);
            }

            @Override
            public void writeString(String string) {
//                int code = (int) character;
                self.writeString(string);
            }

            @Override
            public void setKeyboardType(String string) {
//                int code = (int) character;
                self.setKeyboardType(string);
            }
        });
    }

    public interface MyInterface {
        public void composite(int character);
        public void write(int character);
        public void writeString(String string);
        public void setKeyboardType(String string);
    }

    @Override
    public void onBindInput (){
//        Toast.makeText(this, "keyboard createinputconnection", Toast.LENGTH_SHORT).show();
        initSensor();
        initPreferences();
        createKeyboard();
        setKeyboardType("qwerty");
        sensorCalculations = new KeyboardSensor(this.getApplicationContext());
        Log.e(TAG, "onBindInput >>");

//        Toast.makeText(this, "onBindInput", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onWindowHidden(){
                Log.e(TAG, "onWindowHidden >>");
        shown = false;
//        Toast.makeText(this, "keyboard hidden", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onWindowShown() {
        super.onWindowShown();
        Log.e(TAG, "onWindowShown >>");
        shown = true;
        keyboardService.setKeyboardJustOpened(true);
//                Toast.makeText(this, "onWindowShown", Toast.LENGTH_SHORT).show();

//        Calculations.initAzimut();
        initSensor();
        sensorCalculations = new KeyboardSensor(this.getApplicationContext());
        if (getCurrentInputConnection() != null){
            //TODO
//            keyboardService.lastCode = getCurrentInputConnection().getTextBeforeCursor(1,0).toString();
//            keyboardService.setPredictionKey();
        }
    }

    @Override
    public View onCreateInputView() {
        initPreferences();

        createKeyboard();
        setKeyboardType("qwerty");

//        Toast.makeText(this, "keyboard vsibiellee", Toast.LENGTH_SHORT).show();

        return keyboardService.view;
    }


    private void initSensor() {
        Context context = getLayoutInflater().getContext();
//        Calculations = new SensorCalculations(preferences.getValue("keyboard_type"));
//        Calculations.setPreferences(preferences);

        sensorCalculations = new KeyboardSensor(this.getApplicationContext());


        senSensorManager = (SensorManager) getSystemService(context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = senSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        senSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        playClick(primaryCode);
        kv.invalidateAllKeys();
        write(primaryCode);
    }

    private static boolean contains(int[] arr, int item) {
        for (int n : arr) {
            if (item == n) {
                return true;
            }
        }
        return false;
    }

    private void writeString(String code_){
        InputConnection ic = getCurrentInputConnection();
//        Log.e(TAG, "setting num >>");

        switch(code_.toLowerCase()){
            case "num":
                    setKeyboardType("num");
                break;

            default:
                if (caps) {
                    code_ = code_.toUpperCase();
                }
                if (ic != null){
//            keyboardService.create();
                    ic.commitText(code_, 1);
                }else{
//            keyboardService.destroy();
                }

        }

    }

    private void writeText(int code_){
//        Log.e(TAG, "setting num >>" + code_);

        InputConnection ic = getCurrentInputConnection();
        char code;
        code = (char) code_;

//        Log.e(TAG, "setting num >>" + code_);

        if (Character.isLetter(code) && caps) {
            code = Character.toUpperCase(code);
        }
        if (ic != null){
//            keyboardService.create();
            ic.commitText(String.valueOf(code), 1);
        }else{
//            keyboardService.destroy();
        }
    }

    private void compositeText(int code_){
        InputConnection ic = getCurrentInputConnection();
        char code;
        code = (char) code_;
        if (Character.isLetter(code) && caps) {
            code = Character.toUpperCase(code);
        }

        if (ic != null){
//            keyboardService.create();
            ic.setComposingText(String.valueOf(code), 1);
        }else{
//            keyboardService.destroy();
        }
    }

    private boolean write(int primaryCode) {
        boolean write = true;
        char code;
        InputConnection ic = getCurrentInputConnection();
        switch (primaryCode){
            case -200:
                write = true;
                break;

            case -333:
                write = false;
//                setActiveKeyboard(qwerty_special_chars);
                break;

            case -90:
                write = false;
//                setActiveKeyboard(aeiou);
                break;

            case -99:
                write = false;
//                setActiveKeyboard(qwerty);
                break;

            case -100:
                write = false;
//                setActiveKeyboard(qwerty_1);
                break;

            case -101:
                write = false;
//                setActiveKeyboard(qwerty_2);
                break;

            case -102:
                write = false;
//                setActiveKeyboard(qwerty_3);
                break;

            case -103:
                write = false;
//                setActiveKeyboard(qwerty_4);
                break;

            case -104:
                write = false;
//                setActiveKeyboard(qwerty_5);
                break;

            case -123:
                write = false;
//                setActiveKeyboardsetActiveKeyboard(nums);
                break;

            default:
                selectedCode = primaryCode;
                write = true;
                break;
        }

        if (!write)
            return write;

        switch(primaryCode) {
            case Keyboard.KEYCODE_DELETE:
                ic.deleteSurroundingText(1, 0);
                break;
            case Keyboard.KEYCODE_SHIFT:
                caps = !caps;
//                tv.setAllCaps(true);
//                activeKeyboard.setShifted(caps);
//                kv.invalidateAllKeys();
                break;
            case Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            case 32:
                writeText(primaryCode);
                break;
            case -200:
                    write(selectedCode);
                break;
            default:
                writeText(primaryCode);

                if (contains(ifRedirectHome, primaryCode)){
//                    setActiveKeyboard(qwerty);
                }

                break;
        }

        return write;
    }

    private void playClick(int keyCode){
        AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);
        switch(keyCode){
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                break;
            case Keyboard.KEYCODE_DONE:
            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default: am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }

    @Override
    public void onPress(int primaryCode) {
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
//        setActiveKeyboard(nums);
    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeUp() {
//        setActiveKeyboard(qwerty);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (!shown){
            return ;
        }
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;

        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;

        sensorCalculations
                .setPreferences(preferences)
                .setKeyboard(preferences.getValue("keyboard_type"))
                .setSensorEvent(event)
                .setGravity(mGravity)
                .setGeomagnetic(mGeomagnetic)
                .calculate();

        keyboardService.markButton(sensorCalculations.getCode(), sensorCalculations.getPrecision());


//        Calculations.setKeys(preferences.getValue("keyboard_type"));
//        Calculations.setSensor(event);
//        SensorCode = Calculations.getCode();
//
//        int precision = Calculations.getPrecision();
//        keyboardService.markButton(SensorCode, precision);
//        selectedCode = codeFromSensor;
//        compositeText(selectedCode);

//        InputConnection ic = getCurrentInputConnection();
//
//        if (ic != null) {

//            keyboardService.create();
//            ic.setComposingText(precision + " :: " + Calculations.getX()+ " :: " + SensorCode + " :: " + Calculations.getColumn() + "!", 1);
//            ic.setComposingText(precision + " :: " + (int) (Calculations.columnFloat*4)+ " :: " + SensorCode + " :: " + Calculations.getColumn() + "!", 1);
//            ic.setComposingText(precision + " :: " + Calculations.getAzimutOrientation()+ " :: " + Calculations.getX() + " :: " + Calculations.initAzimut + "!", 1);
//        }else{
////            keyboardService.destroy();
//        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.KEYBOARDHIDDEN_NO) {
//            Toast.makeText(this, "keyboard visible", Toast.LENGTH_SHORT).show();
        } else if (newConfig.hardKeyboardHidden == Configuration.KEYBOARDHIDDEN_YES) {
//            Toast.makeText(this, "keyboard hidden", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onDestroy()
    {
//        Toast.makeText(this, "keyboard destroyed", Toast.LENGTH_SHORT).show();

        super.onDestroy();
//        keyboardService.onDestroy();
    }

    public void setKeyboardType(String keyboardType) {
//        initSensor();
//        Toast.makeText(this, "keyboard keyboart" + keyboardType, Toast.LENGTH_SHORT).show();

        this.keyboardType = keyboardType;

        keyboardService.setLayaoutInflater(getLayoutInflater());
//        getWindow().getWindow().addContentView(keyboardService.view, params);
//        getWindow().
        keyboardService.setKeyboard(this.keyboardType);
        setInputView(keyboardService.view);
        updateInputViewShown();
    }

    public Preferences initPreferences() {
        preferences = new Preferences(getApplicationContext());
        return preferences;
    }
}