package com.example.tomis.keyboard3d;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.tomis.keyboard3d.classes.Preferences;
import com.example.tomis.keyboard3d.objects.KeyboardCodes;

/**
 * Created by tomis on 2/7/2016.
 */
public class SensorCalculations {
    private KeyboardCodes keys;
    private Sensor sensor;
    private SensorEvent event;
    private static int code = 0;
    private long lastUpdate = 0;
    private float last_pitch=0, last_roll=0, last_azimut=0, last_yaw=0, default_orientation=0;
    private static final float SHAKE_THRESHOLD = 20;
    private static final float DISTANCE_THRESHOLD = (float)0.0;
    protected static final String TAG = "FrequencyAdapter";

    private float[] mGravity;
    private float[] mGeomagnetic;

    public float azimutOrientation;
    public float pitchOrientation;
    public float azimut, initAzimut=1000;
    public float pitch;
    public float roll;
    public float yaw;

    public int row=0,column=0;
    private Preferences preferences;
    private int sensitivityY;
    private int sensitivityX;
    private int precision;
    private int precisions = 4;
    public float columnFloat;

    public SensorCalculations(String keyboard){
        keys = new KeyboardCodes(keyboard);

    }

    public void setKeys(String keyboard){
        keys = new KeyboardCodes(keyboard);
    }
    
    public void setPreferences(Preferences preferences){
        this.preferences = preferences;
        setSensitivity();

//        keys.setKeyboard(preferences.getValue("keyboard_type"));

    }

    public void setSensitivity(){
        this.sensitivityX = Integer.parseInt(this.preferences.getValue("sensitivityX"));
        this.sensitivityY = Integer.parseInt(this.preferences.getValue("sensitivityY"));
    }

    public String getCode(){

        return keys.getCode(this.getRow(), this.getColumn());
//        return Integer.parseInt(preferences.getValue("sensitivity"));
//        return this.code;
    }

    public int getParams() {
        return this.code;
    }

    public float getAzimutOrientation(){
        return this.azimutOrientation;
    }

    public float getX(){
        float value = this.azimutOrientation;
        if(this.initAzimut == 1000){
            return value;
        }

        value = this.initAzimut - this.azimutOrientation + 90;

        if (value > 360){
            value = value - 360;
        }

        if (value < 0){
            value = value + 360;
        }

        return value;
    }

    public float getY(){
        float value = this.pitchOrientation - 115;

        if (value <1){
            value = 0;
        }
        return value;
    }

    public int getColumn(){
        return Math.abs(this.column);
    }

    public int getRow(){
        return Math.abs(this.row);
    }

    public int getPrecision() {
        return (int) (this.columnFloat*4);
    }

    public float getPitchOrientation(){
        return this.pitchOrientation;
    }

    public void setSensor(SensorEvent event){
        this.init(event);
        this.calculate();
        this.calculateCode();
    }

    public void initAzimut(){
        this.initAzimut = this.getAzimutOrientation();
    }
    private void init(SensorEvent event) {
        this.sensor = event.sensor;
        this.event = event;
    }

    private void setDefaults(){
    }

    private void calculateCode() {
        calculateRow();
        calculateColumn();
        calculatePrecision();
    }

    private void calculatePrecision() {
        setSensitivity();
        int columns = Integer.parseInt(preferences.getValue("keyboard_columns")) - 1;
        float azimutPerKey = this.sensitivityX/columns;
        this.columnFloat =((this.getX()/azimutPerKey) - this.column);
    }

    private void calculateColumn() {
        setSensitivity();

        int columns = Integer.parseInt(preferences.getValue("keyboard_columns"));
        float azimutPerKey = this.sensitivityX/columns;
        this.column = (int) (this.getX()/azimutPerKey);

        columns = columns-1;
        if (preferences.getValue("repeat_X").trim().equals("true")){
            if (this.column > columns){
                this.column = this.column - columns;
            }

            if (this.column < 0){
                this.column = this.column + columns;
            }
        }else {
            if (this.column > columns){
                this.column = columns;
            }

            if (this.column < 0){
                this.column = 0;
            }
        }
        Log.e(TAG, this.sensitivityX + "calculatin+ons num >>" + this.azimut + " :: " + this.getX() + " :: " + azimutPerKey);

    }

    private void calculateRow() {
        setSensitivity();

        int rows = Integer.parseInt(preferences.getValue("keyboard_rows")) - 1;
        float yPerKey = this.sensitivityY/rows;
        this.row = (int) Math.ceil(this.getY()/yPerKey);

        if (preferences.getValue("repeat_Y").trim().equals("true")){
            if (this.row > rows){
                this.row = this.row - rows;
            }

            if (this.row < 0){
                this.row = this.row + rows;
            }
        }else {
            if (this.row > rows){
                this.row = rows;
            }

            if (this.row < 0){
                this.row = 0;
            }
        }

    }

    private void calculateOrientations() {
        this.calculateAzimutOrientation();
        this.calculatePitchOrientation();
    }

    private void calculateAzimutOrientation(){
        this.azimutOrientation = (-this.azimut*360/(2*3.14159f)) + 180;
    }

    private void calculatePitchOrientation(){
        this.pitchOrientation = (-this.pitch*360/(2*3.14159f)) + 90;
    }

    private void calculate(){
        long curTime = System.currentTimeMillis();

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;

        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;

        if (mGravity != null && mGeomagnetic != null) {

            if ((curTime - this.lastUpdate) > 100) {

                long diffTime = (curTime - this.lastUpdate);
                this.lastUpdate = curTime;

                float R[] = new float[9];
                float I[] = new float[9];
                boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);

                if (success) {
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(R, orientation);

                    float distance_x = this.last_azimut - orientation[0];
                    float distance_y = this.last_pitch - orientation[1];
                    float distance_z = this.last_roll - orientation[2];
                    float distance_yaw = this.last_yaw - mGravity[0];

                    float distance = Math.abs(distance_x + distance_y + distance_z);
                    float speed = Math.abs(distance)/ diffTime * 10000;

                    if (speed > SHAKE_THRESHOLD || Math.abs(distance) > DISTANCE_THRESHOLD) {
                        this.azimut = orientation[0]; // orientation contains: azimut, pitch and roll
                        this.pitch = orientation[1];
                        this.roll = orientation[2];
                        this.yaw = mGravity[0];

                        this.calculateOrientations();

                        if (this.initAzimut == 1000){
                            this.initAzimut();
                        }
                    }


                    this.last_azimut = orientation[0];
                    this.last_pitch = orientation[1];
                    this.last_roll = orientation[2];
                }

            }

        }
    }



}
