package com.example.tomis.keyboard3d;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by tomis on 2/7/2016.
 */
public class Main extends Activity implements SensorEventListener {

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private Sensor magnetometer;

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private float now_x = 0 , now_y = 0, now_z = 0;
    private static final float SHAKE_THRESHOLD = 20;
    private static final float DISTANCE_THRESHOLD = (float)0.4;

    private float[] mGravity;
    private float[] mGeomagnetic;
    private Float azimut;

    private SensorCalculations Calculations;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainui);

        Calculations = new SensorCalculations("qwerty");

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = senSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        senSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_UI);

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mySensor = event.sensor;
        TextView text = (TextView)findViewById(R.id.editText);
        Calculations.setSensor(event);
//            int code = Calculations.getCode();
            text.setText("azimut: "+Calculations.getAzimutOrientation()+
                            ",pitch: "+Calculations.pitch);
//        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//            float x = event.values[0];
//            float y = event.values[1];
//            float z = event.values[2];
//            Calculations.setSensor(event);
//            int code = Calculations.getCode();
//            text.setText("now_x: "+Calculations.now_x+
//                            ",x: "+x);
//        }
//
//        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
//            mGravity = event.values;
//
//        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
//            mGeomagnetic = event.values;
//
//        if (mGravity != null && mGeomagnetic != null) {
//            float R[] = new float[9];
//            float I[] = new float[9];
//            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
//            if (success) {
//                float orientation[] = new float[3];
//                SensorManager.getOrientation(R, orientation);
//                azimut = orientation[0]; // orientation contains: azimut, pitch and roll
//                text.setText("AZIMUT: "+(-azimut*360/(2*3.14159f))+" 2: " +orientation[1] + " 3: " +orientation[2]);
//            }
//        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
}