package com.example.tomis.keyboard3d.classes;

import android.content.Context;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.tomis.keyboard3d.R;
import com.example.tomis.keyboard3d.SimpleIME;
import com.example.tomis.keyboard3d.objects.KeyboardCodes;

/**
 * Created by tomis on 2/16/2016.
 */
public class KeyboardService implements View.OnClickListener {
    private WindowManager windowManager;
    private Context context;

    public View view;
    public int rows,columns;


    private LayoutInflater inflater;
    private Preferences preferences;
    private KeyboardService self;
    private String keyText = "";
    private SimpleIME.MyInterface onClickCallBack_;
    private Button[] buttons;
    public String currentCode="", lastCode="";
    public String[] codeStates;
    public FrequencyAdapter frequencyAdapter;
    public boolean caps = false;
    protected static final String TAG = "FrequencyAdapter";
    public Button predictionButton;
    private long lastUpdateDwell = 0;
    private String dwellCode;
    private String lastDwellClicked;
    private boolean keyboardJustOpened;


    public void Keyboard(){
    }

    public void setLayaoutInflater(LayoutInflater inflater){
        this.inflater = inflater;
    }

    public void setPreferences(Preferences preferences){
        this.preferences = preferences;
    }

    public void setKeyboard(String keyboard){
//        Log.e(TAG, "setting num >>" + keyboard);

    if (preferences.getValue("keyboard_divided_option").toString().equals("yes") && keyboard.equals("qwerty")){
        keyboard = "qwerty_divided_view";
    }
        switch(keyboard.toLowerCase()){

            case "qwerty":
                    this.setQwerty();
                break;

            case "qwerty_divided_view":
                    this.setDividedQwerty();
                break;

            case "num":
                this.setNum();
                break;

            case "! ?":
            case ": ;":
            case ". ,":
            case "x c":
            case "v b":
            case "n m":
            case "a s":
            case "d f":
            case "g h":
            case "j k":
            case "q w":
            case "e r":
            case "t y":
            case "u i":
            case "o p":
                this.setChosenDividedKeyboard(keyboard);
                break;

            default:
                this.setQwerty();

        }

        frequencyAdapter = new FrequencyAdapter(this.context)
                .createDatabase();
//                .open();

        this.setListener();
    }

    private void setNum() {
        //               this.view = inflater.inflate(R.layout.num_keyboard, null);
//                            Log.e(TAG, "setting num >>");
        this.preferences.setValue("num", "keyboard_type");
        this.view = inflater.inflate(R.layout.num, null);
        this.preferences.setValue("5", "keyboard_rows");
        this.preferences.setValue("5", "keyboard_columns");

    }

    private void setQwerty() {
        this.preferences.setValue("qwerty", "keyboard_type");
        this.view = inflater.inflate(R.layout.keyboard, null);
        this.preferences.setValue("5", "keyboard_rows");
        this.preferences.setValue("10", "keyboard_columns");
    }

    private void setDividedQwerty(){
        this.preferences.setValue("qwerty_divided_view", "keyboard_type");
        this.view = inflater.inflate(R.layout.qwerty_divided_view, null);
        this.preferences.setValue("5", "keyboard_rows");
        this.preferences.setValue("5", "keyboard_columns");
    }

    private void setChosenDividedKeyboard(String keyboard){
        keyboard = keyboard.trim();
        this.preferences.setValue("4", "keyboard_rows");
        this.preferences.setValue("2", "keyboard_columns");

        switch(keyboard.toLowerCase()){
            case "! ?":
                this.preferences.setValue("qwerty_divided_view_question", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_question, null);
                break;
            case ": ;":
                this.preferences.setValue("qwerty_divided_view_dot_dot", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_dot_dot, null);
                break;
            case ". ,":
                this.preferences.setValue("qwerty_divided_view_dot", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_dot, null);
                break;
            case "x c":
                this.preferences.setValue("qwerty_divided_view_xc", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_xc, null);
                break;
            case "v b":
                this.preferences.setValue("qwerty_divided_view_vb", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_vb, null);
                break;
            case "n m":
                this.preferences.setValue("qwerty_divided_view_nm", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_nm, null);
                break;
            case "a s":
                this.preferences.setValue("qwerty_divided_view_as", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_as, null);
                break;
            case "d f":
                this.preferences.setValue("qwerty_divided_view_df", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_df, null);
                break;
            case "g h":
                this.preferences.setValue("qwerty_divided_view_gh", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_gh, null);
                break;
            case "j k":
                this.preferences.setValue("qwerty_divided_view_jk", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_jk, null);
                break;
            case "q w":
                this.preferences.setValue("qwerty_divided_view_qw", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_qw, null);
                break;
            case "e r":
                this.preferences.setValue("qwerty_divided_view_er", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_er, null);
                break;
            case "t y":
                this.preferences.setValue("qwerty_divided_view_ty", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_ty, null);
                break;
            case "u i":
                this.preferences.setValue("qwerty_divided_view_ui", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_ui, null);
                break;
            case "o p":
                this.preferences.setValue("qwerty_divided_view_op", "keyboard_type");
                this.view = inflater.inflate(R.layout.qwerty_divided_view_op, null);
                break;
        }

    }

    private boolean canClick (){
        boolean result = true;

        if (preferences.getValue("keys_click") == null){
            return result;
        }
        if (preferences.getValue("keys_click").equals("off")) {
            result = false;
        }
        return result;
    }

    private void setListener(){
        int counter = 0;
        self = this;
        this.buttons = new Button[501];

        predictionButton = (Button) view.findViewById(this.context.getResources().getIdentifier("predictionKey", "id", this.context.getPackageName()));
        for (int i = 0; i < 70; i++) {
            int id = this.context.getResources().getIdentifier("bKey" + i, "id", this.context.getPackageName());
            buttons[counter] = (Button) view.findViewById(id);

            if (buttons[counter] != null) {
                if (canClick()){
                    buttons[counter].setOnClickListener(this);
                }
                counter++;
            }

            id = this.context.getResources().getIdentifier("specialKey" + i, "id", this.context.getPackageName());
            buttons[counter] = (Button) view.findViewById(id);

            if (buttons[counter] != null) {
                if (canClick()){
                    buttons[counter].setOnClickListener(this);
                }
                counter++;
            }

            id = this.context.getResources().getIdentifier("num" + i, "id", this.context.getPackageName());
            buttons[counter] = (Button) view.findViewById(id);

            if (buttons[counter] != null) {
                if (canClick()){
                    buttons[counter].setOnClickListener(this);
                }
                counter++;
            }
        }

        checkCaps();
    }

    public void markButton(String code, int precision){
        Button button;

//        this.keyText = code;
//        String lastCode_ = lastCode;
//        lastCode =this.currentCode;

        for (int i=0; i<70; i++){

            if (buttons[i] != null) {

                if (this.buttons[i].getText().toString().toLowerCase().equals(code.toLowerCase()) && !code.toLowerCase().equals("space")){
                    new ButtonBackground(this.context)
                            .button(this.buttons[i])
                            .precision(precision)
                            .render();
                }
                else if (this.buttons[i].getText().toString().toLowerCase().equals(code.toLowerCase()) && code.toLowerCase().equals("space")){
                    new ButtonBackground(this.context)
                            .button(this.buttons[i])
                            .precision(1)
                            .render();

                    new ButtonBackground(this.context)
                            .button(predictionButton)
                            .precision(0)
                            .render();
                }
                else{
                    new ButtonBackground(this.context)
                            .button(this.buttons[i])
                            .precision(0)
                            .render();

                    new ButtonBackground(this.context)
                            .button(predictionButton)
                            .precision(0)
                            .render();
                }
            }
        }

        if (code.toLowerCase().equals("prediction0")){

            new ButtonBackground(this.context)
                    .button(predictionButton)
                    .precision(2)
                    .render();

            if (!predictionButton.getText().toString().equals("prediction...")){
                this.currentCode = predictionButton.getText().toString();
            }

//            if (lastCode.equals(currentCode)){
//                lastCode = lastCode_;
//            }
        }
        else{
            this.currentCode = code;

//            if (lastCode.equals(currentCode)){
//                lastCode = lastCode_;
//            }
        }
        checkDwell(currentCode);
    }


    private void checkDwell(String code) {
        long curTime = System.currentTimeMillis();

        if (preferences.getValue("dwellTime") == null){
            return ;
        }

        if (getKeyboardJustOpened()){
            dwellCode = code;
            setKeyboardJustOpened(false);
            this.lastUpdateDwell = 0;
        }

        if (dwellCode != null && lastDwellClicked != null && dwellCode.equals(code) && lastDwellClicked.equals(code)){

            return ;
        }

        if (!code.equals(dwellCode)){
            this.lastUpdateDwell = curTime;
            dwellCode = code;
            lastDwellClicked = null;
            vibrate();
        }

        if (((curTime - this.lastUpdateDwell) > Integer.parseInt(preferences.getValue("dwellTime"))) && !getKeyboardJustOpened() && this.lastUpdateDwell !=0) {

            if (preferences.getValue("dwellOption") != null && preferences.getValue("dwellOption").equals("off")){
                return ;
            }

            self.setText(code.toString());
            self.onClickCallBack();
            this.lastDwellClicked = code;
        }

    }

    public void setContext(Context context){
        this.context = context;
    }

    public void setWindowManager(WindowManager windowManager){
        this.windowManager = windowManager;
    }

    public void setPredictionKey(){
        String predictionKey = frequencyAdapter.getPredictionKey(lastCode, this.currentCode);
        if (predictionKey.length() > 0){
            if (predictionButton != null){
                predictionButton.setText(predictionKey.toString());
            }
        }
    }

    public void insertPredictionKey(){
        if (!keyText.equals("CAPS") && keyText.length()>0){

            frequencyAdapter.insertPredictionKey(lastCode, keyText.toString().trim());
        }
    }

    public void toggleCaps(){
        if (caps){
            caps = false;
            keysToLowerCase();
        }else {
            caps = true;
            keysToUpperCase();
        }
    }

    public void checkCaps(){
        if (!caps){
            keysToLowerCase();
        }else {
            keysToUpperCase();
        }
    }

    public void keysToUpperCase(){
        for (int i=0; i<70; i++){

            if (buttons[i] != null) {
                String text = buttons[i].getText().toString();
                if (text == "SPACE"){
                    continue;
                }
                buttons[i].setText(buttons[i].getText().toString().toUpperCase());
            }
        }
    }

    public void keysToLowerCase(){
        for (int i=0; i<70; i++){

            if (buttons[i] != null) {
                buttons[i].setText(buttons[i].getText().toString().toLowerCase());
            }
        }
    }

    public void setText(String text){
        lastCode = this.getText();
        setPredictionKey();

        switch (text.toLowerCase()){
//            case "caps":

            case "upiši":
                this.keyText = this.currentCode;
                break;

            default:
                this.keyText = text;
                break;
        }


        if (this.keyText.toLowerCase().equals("caps")){
             toggleCaps();
        }
        insertPredictionKey();
    }

    public String getText(){
        return self.keyText;
    }


    public void destroy() {
//        if (!destroyed){
//            destroyed = true;
//            if (chatHead != null){
//                try{
//                    windowManager.removeView(chatHead);
//                }catch (IndexOutOfBoundsException  e){
//                }
//            }
//        }
    }

    public void setOnClick(SimpleIME.MyInterface callback){
        this.onClickCallBack_ = callback;
    }

    public void onClickCallBack(){

        if (this.getText().length() <= 0){
            return;
        }

        if (this.getText().toLowerCase().equals("num")){
            onClickCallBack_.setKeyboardType("num");
            return;
        }

        if (this.getText().toLowerCase().equals("qwerty")){
            onClickCallBack_.setKeyboardType("qwerty");
            return;
        }

        if (this.getText().toLowerCase().equals("! ?")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals(": ;")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("x c")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("v b")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("n m")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("a s")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("d f")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("g h")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("j k")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("q w")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("e r")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("t y")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }
        if (this.getText().toLowerCase().equals("u i")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }

        if (this.getText().toLowerCase().equals("o p")){
            onClickCallBack_.setKeyboardType(this.getText());
            return;
        }






//        Log.e(TAG, "clicked keyboard type >>" + preferences.getValue("keyboard_type"));


        KeyboardCodes keyboardCodes = new KeyboardCodes(preferences.getValue("keyboard_type").toString());
//        keyboardCodes.setKeyboard(preferences.getValue("keyboard_type"));

        int code =keyboardCodes.toCode(this.getText());

        onClickCallBack_.write(code);
//        onClickCallBack_.writeString(this.getText().toString());
    }

    @Override
    public void onClick(View v) {

        if (v instanceof Button) {
            Button key = (Button) v;
            new ButtonBackground(this.context)
                    .button(key)
                    .precision(1)
                    .render();

            self.setText(key.getText().toString());
//            Log.e(TAG, "getTestDataasdasda >>" + this.getText());

            self.onClickCallBack();
        }

    }

    public void vibrate(){
        int vibrateTime = 100;
        Log.e(TAG, preferences.getValue("vibrateTime") + " sadasdasdasdsadsad as ad asd sa :: " + preferences.getValue("vibrateOption"));

        if (preferences.getValue("vibrateTime")!=null){
            vibrateTime = Integer.parseInt(this.preferences.getValue("vibrateTime"));
        }

        if (preferences.getValue("vibrateOption")!=null && preferences.getValue("vibrateOption").equals("on")){
            Vibrator v = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);


            v.vibrate(vibrateTime);
        }
    }

    public void setKeyboardJustOpened(boolean keyboardJustOpened) {
        this.keyboardJustOpened = keyboardJustOpened;
    }

    public boolean getKeyboardJustOpened() {
        return this.keyboardJustOpened;
    }
}
