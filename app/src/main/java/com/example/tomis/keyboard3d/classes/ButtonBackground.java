package com.example.tomis.keyboard3d.classes;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.widget.Button;

import com.example.tomis.keyboard3d.R;

/**
 * Created by tomis on 4/2/2016.
 */
public class ButtonBackground {
    public Context context;
    public Button button;

    private int precision;
    private int colorTo;
    private StateListDrawable currentColorId;
    private ColorDrawable currentColor;
    private int colorFrom;
    private int returnPrecision = -1;
    private int duration;

    public ButtonBackground(Context context){
        this.context = context;
    }

    public ButtonBackground button(final Button button){
        this.button = button;
        return this;
    }

    public ButtonBackground precision(final int precision){
        this.precision = precision;
        return this;
    }

    public ButtonBackground returnPrecision(final int precision){
        this.returnPrecision = precision;
        return this;
    }
    public void render (){
        this.initColor();
        int colorTo = ContextCompat.getColor(context, this.colorTo);

        this.button.setBackgroundColor(colorTo);
    }

    public void animate(final int duration){
        this.duration = duration;
        this.initColor();

//        int colorFrom = ContextCompat.getColor(context, this.currentColor);
        int colorTo = ContextCompat.getColor(context, this.colorTo);
        int colorFrom = ContextCompat.getColor(context, this.colorFrom);

        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), this.button.getBackground().getColorFilter(), colorTo);
        colorAnimation.setDuration(duration); // milliseconds

        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                button.setBackgroundColor((int) animator.getAnimatedValue());
            }

        });

        colorAnimation.addListener(new ValueAnimator.AnimatorListener(){

            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (returnPrecision > -1){
                    int precision = returnPrecision;
                    returnPrecision = -1;

                    precision(precision)
                            .animate(duration);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        colorAnimation.start();
    }

    private void initColor() {
        this.colorFrom = R.color.button_state_0;

        switch (this.precision){
            case 0:
                this.colorTo = R.color.button_state_0;
                break;
            case 1:
                this.colorTo = R.color.button_step_1;
                break;
            case 2:
                this.colorTo = R.color.button_step_2;
                break;
            case 3:
                this.colorTo = R.color.button_step_3;
                break;
            case 4:
                this.colorTo = R.color.button_step_4;
                break;

            default:
                this.colorTo = R.color.button_step_4;
        }

    }


}
