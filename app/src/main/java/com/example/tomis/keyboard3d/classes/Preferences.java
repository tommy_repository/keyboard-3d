package com.example.tomis.keyboard3d.classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by tomis on 2/19/2016.
 */


public class Preferences {

    public static final String PREFS_NAME = "PREFERENCES_";

    private Context context;
    protected static final String TAG = "FrequencyAdapter";

    public Preferences(Context context) {
        super();

        this.context = context;
        this.setDefaultValues();
    }

    private void setDefaultValues() {
        if (this.getValue("settings_is_set") == null){
            this.setValue("qwerty", "keyboard_type");
            this.setValue("false", "keyboard_divided_option");
            this.setValue("180", "sensitivityX");
            this.setValue("60", "sensitivityY");
            this.setValue("true", "repeat_X");
            this.setValue("yes", "keyboard_divided");
            this.setValue("Casual", "keyboard_themes");
            this.setValue("500", "dwellTime");
            this.setValue("off", "dwellOption");
            this.setValue("off", "vibrateOption");
            this.setValue("100", "vibrateTime");
            this.setValue("set", "settings_is_set");
//            this.setValue(true, "pref_repeat_keyboard");
        }

        if (this.getValue("sensitivityX") != null){
            if (this.getValue("sensitivityX").toString() == "-1"){
                Log.e(TAG, " sadasdasdasdsadsad as ad asd sa :: ");

            }
        }

        Log.e(TAG, this.getValue("sensitivityX") + " setting :: " + this.getValue("sensitivityY") + " num >>" + this.getValue("settings_is_set"));

//        if (this.getValue("keyboard_type") == null){
//            this.setValue("qwerty", "keyboard_type");
//        }
//
//        if (this.getValue("keyboard_divided_option") == null){
//            this.setValue("false", "keyboard_divided_option");
//        }
//
//        if (this.getValue("sensitivityX") == null){
//            this.setValue("180", "sensitivityX");
//        }
//
//        if (this.getValue("sensitivityY") == null){
//            this.setValue("60", "sensitivityY");
//        }
//
//        if (this.getValue("repeat_X") == null){
//            this.setValue("true", "repeat_X");
//        }
//
//        if (this.getValue("repeat_Y") == null){
//            this.setValue("false", "repeat_Y");
//        }
//
//        if (this.getValue("keyboard_divided") == null){
//            this.setValue("yes", "keyboard_divided");
//        }

//        if (this.getValue("pref_repeat_keyboard") == null){
//            this.setValue(true, "pref_repeat_keyboard");
//        }
//
//        if (this.getValue("keyboard_themes") == null){
//            this.setValue("Casual", "keyboard_themes");
//        }
//        this.setValue("180", "sensitivityX");
//        this.setValue("90", "sensitivityY");
//        this.setValue("true", "repeat_X");
//        this.setValue("false", "repeat_Y");
    }

    public void setValue(String text, String key) {
        SharedPreferences settings;
        Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        editor = settings.edit();
        editor.putString(key, text);

        editor.commit();
    }

    public String getValue(String key) {
        SharedPreferences settings;
        String text;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(key, null);
        return text;
    }

    public void clearSharedPreference() {
        SharedPreferences settings;
        Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
    }

    public void removeValue(String key) {
        SharedPreferences settings;
        Editor editor;

        settings = this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.remove(key);
        editor.commit();
    }
}