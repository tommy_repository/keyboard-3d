package com.example.tomis.keyboard3d.classes;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.util.Log;

import com.example.tomis.keyboard3d.objects.KeyboardCodes;

/**
 * Created by tomis on 4/19/2016.
 */
public class KeyboardSensor {

    private Context context;
    private Preferences preferences;
    private KeyboardCodes keyboardType;
    private Sensor sensor;
    private SensorEvent event;
    private float[] mGravity;
    private float[] mGeomagnetic;

    protected static final String TAG = "FrequencyAdapter";


    private long lastUpdate = 0;
    public float azimut;
    public float pitch;
    public float roll;
    public float yaw;
    private float[] gravity;
    private float[] geomagnetic;
    private float pitchOrientation;
    private float azimutOrientation;
    private float initAzimut = 0;
    private int row;
    private int column;
    private int sensitivityX;
    private int sensitivityY;
    private int keyboard_columns;
    private int keyboard_rows;
    private String repeat_Y;
    private String repeat_X;
    private float columnFloat;

    public KeyboardSensor(Context context){
        this.context = context;
    }

    public KeyboardSensor setSensorEvent(SensorEvent event){
        this.sensor = event.sensor;
        this.event = event;

        return this;
    }

    public KeyboardSensor setPreferences(Preferences preferences){
        this.preferences = preferences;
        return this;
    }

    public KeyboardSensor setKeyboard(String keyboardType){
        this.keyboardType = new KeyboardCodes(keyboardType);
        return this;
    }

    public KeyboardSensor setGravity(float[] gravity) {
        this.gravity = gravity;
        return this;
    }

    public KeyboardSensor setGeomagnetic(float[] geomagnetic) {
        this.geomagnetic = geomagnetic;
        return this;
    }

    public int getRow(){
        return this.row;
    }

    public int getColumn(){
        return this.column;
    }

    public int getPrecision() {
//        return (int) (this.columnFloat);
        return 3;
    }

    public void initAzimut(){
        this.setInitAzimut();
    }

    public void setInitAzimut(){
        this.initAzimut = this.azimutOrientation;
    }

    public String getCode(){
        int row = this.getRow();
        int column = this.getColumn();

        if (row < 0){
            row = 0;
        }
        if (column < 0){
            column = 0;
        }
        if (column >= keyboard_columns){
            column = keyboard_columns-1;
        }

        if (row >= keyboard_rows){
            row = keyboard_rows-1;
        }
//        if (row > keyboard_rows){
//            row = keyboard_rows-1;
//        }

        return this.keyboardType.getCode(row, column);
    }
    /*
    Sensor calculations
     */

    public void calculate(){
        preferenceCalculations();
        sensorCalculations();
        azimutCalculations();
        pitchCalculations();
        rowCalculations();
        columnCalculations();
        calculatePrecision();

    }

    private void preferenceCalculations() {
        this.sensitivityX = Integer.parseInt(this.preferences.getValue("sensitivityX"));
        this.sensitivityY = Integer.parseInt(this.preferences.getValue("sensitivityY"));
        this.keyboard_columns = Integer.parseInt(preferences.getValue("keyboard_columns"));
        this.keyboard_rows = Integer.parseInt(preferences.getValue("keyboard_rows"));
        this.repeat_Y = preferences.getValue("repeat_Y");
        this.repeat_X =preferences.getValue("repeat_X");

        if (repeat_Y == null){
            repeat_Y = "false";
        }
        if (repeat_X == null){
            repeat_X = "false";
        }

//        this.sensor_speed = Integer.parseInt(preferences.getValue("sensor_speed"));


    }

    private void rowCalculations() {
        this.row = 0;

        int rows = keyboard_rows;
        float yPerKey = this.sensitivityY/rows;
        this.row = (int) Math.ceil(this.pitchOrientation/yPerKey) - 1;

        if (this.row >= rows){
            this.row = rows - 1;
        }
//        if (this.repeat_Y.equals("true")){
//
//        }

    }

    private void columnCalculations() {
        this.column = 0;

        int columns = keyboard_columns;
        float xPerKey = this.sensitivityX/columns;
        this.column = (int) Math.ceil(this.azimutOrientation/xPerKey) - 1;

        if (this.repeat_X.equals("true")){
            if (this.column > columns){
                this.column = this.column - columns;
            }

            if (this.column < 0){
                this.column = this.column + columns;
            }
        }else {
            if (this.column > columns){
                this.column = columns;
            }

            if (this.column < 0){
                this.column = 0;
            }

            if (this.column == keyboard_columns && this.azimutOrientation > 285){
                this.column = 0;
            }else if (this.column == keyboard_columns){
                this.column = keyboard_columns - 1;
            }
        }

//        if (this.row >= rows){
//            this.row = rows - 1;
//        }

    }

    private void calculatePrecision() {
        int columns = keyboard_columns;
        float azimutPerKey = this.sensitivityX/columns;
        this.columnFloat =Math.abs((int) ((this.column/azimutPerKey) - this.column));
    }

    private void pitchCalculations() {
        this.pitchOrientation = Math.abs((-this.pitch*360/(2*3.14159f)));

    }

    private void azimutCalculations() {
        float value;
        this.azimutOrientation = (-this.azimut*360/(2*3.14159f)) + 180;

        if (this.initAzimut == 0){
            this.initAzimut = azimutOrientation;
        }

        value = this.azimutOrientation;
        value = this.initAzimut - this.azimutOrientation + (int) (sensitivityX/2);

        if (value > 360){
            value = value - 360;
        }

        if (value < 0){
            value = value + 360;
        }

        this.azimutOrientation = value;

    }

    private void sensorCalculations() {
        long curTime = System.currentTimeMillis();

        if (gravity != null && geomagnetic != null) {

            if ((curTime - this.lastUpdate) > 250) {

                long diffTime = (curTime - this.lastUpdate);
                this.lastUpdate = curTime;

                float R[] = new float[9];
                float I[] = new float[9];
                boolean success = SensorManager.getRotationMatrix(R, I, gravity, geomagnetic);

                if (success) {
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(R, orientation);

                        this.azimut = orientation[0]; // orientation contains: azimut, pitch and roll
                        this.pitch = orientation[1];
                        this.roll = orientation[2];
                        this.yaw = gravity[0];

                }

            }

        }
    }
}
