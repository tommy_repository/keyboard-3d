package com.example.tomis.keyboard3d.objects;

import android.util.Log;

import com.example.tomis.keyboard3d.classes.Preferences;

/**
 * Created by tomis on 2/7/2016.
 */

public class KeyboardCodes {
    private int currKeyboard;
    private String[][] currKeys;
    public String PREDICTION_DEFAULT = "prediction...";
    protected static final String TAG = "FrequencyAdapter";

//    private int[][] currKeys;

    public int[][] keys = {{113,119,101,114,116,121,117},
            {97,115,100,102,103,104,106},
            {122,120,99,118,98,110,109},
            {105,105,111,112,107,108,108},
            {-55,46,-50,-50,44,-5,-5}};

//    public int[][] qwerty = {
//            {-123,-100,-101,-102},
//            {-1,-90,-103,-104},
//            {-5,32,32,-333}
//    };

    public String[][] num = {
            {"=","(","SPACE",")","QWERTY"},
            {"*","+","-","/","%"},
            {"6","7","8","9","0"},
            {"1","2","3","4","5"},
            {"PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty = {
            {"?","!",":","SPACE","SPACE","SPACE","SPACE","SPACE","SPACE","NUM"},
            {"CAPS","z","x","c","v","b","n","m",".",","},
            {"a","s","d","f","g","h","j","k","l","DEL"},
            {"q","w","e","r","t","y","u","i","o","p"},
            {"PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_divided_view = {
            {"! ?",": ;","SPACE",". ,","NUM"},
            {"z","x c","v b","n m","DEL"},
            {"a s","d f","g h","j k","l"},
            {"q w","e r","t y","u i","o p"},
            {"PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_question = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"!", "?"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_dot = {
            {"qwerty", "DEL"},
            {"CAPS", "qwerty"},
            {".", ","},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_dot_dot = {
            {"qwerty", "DEL"},
            {"CAPS", "qwerty"},
            {":", ";"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_qw = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"q", "w"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_er = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"e", "r"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_ty = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"t", "y"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_op = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"o", "p"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_as = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"a", "s"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_df = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"d", "f"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_gh = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"g", "h"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_jk = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"j", "k"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_ui = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"u", "i"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_xc = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"x", "c"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_vb = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"v", "b"},
            {"PREDICTION0","PREDICTION0"}
    };

    public String[][] qwerty_nm = {
            {"SPACE", "DEL"},
            {"CAPS", "qwerty"},
            {"n", "m"},
            {"PREDICTION0","PREDICTION0"}
    };

//    public String[][] qwerty_divided_view = {
//            {"?!",":;","SPACE","SPACE","NUM"},
//            {"CAPSZ","xc","vb","nm",".,"},
//            {"as","df","gh","jk","lDEL"},
//            {"qw","er","ty","ui","op"},
//            {"PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0","PREDICTION0"}
//    };

    public int[][] qwerty_1 = {
            {113,119,101,116},
            {97,115,100,102},
            {-1,-1,-99,-99}
    };

    public int[][] qwerty_2 = {
            {116,122,117,105},
            {103,104,106,107},
            {-1,-1,-99,-99}
    };

    public int[][] qwerty_3 = {
            {111,112,123,125},
            {108,58,91,93},
            {-1,-1,-99,-99}
    };

    public int[][] qwerty_4 = {
            {121,120,99,118},
            {98,110,109,46},
            {-1,-1,-99,-99}
    };

    public int[][] qwerty_5 = {
            {46,44,63,33},
            {40,41,58,59},
            {-1,-1,-99,-99}
    };

    public int[][] aeiou = {
            {97,101,105,111},
            {117,46,44,33},
            {-1,-1,-99,-99}
    };

    public int[][] numbers = {
            {49,50,51,52},
            {53,54,55,56},
            {-99,-5,57,48}
    };

    public int[][] qwerty_special_chars = {
            {39,35,36,35},
            {47,42,43,45},
            {-1,-1,-99,-99}
    };

    public KeyboardCodes(String keyboard){
//        this.preferences = preferences;
        this.setKeyboard(keyboard);
//        this.currKeys = qwerty;
    }

//    public void setCurrKeyboard(String keyboardName){
//        switch(keyboardName.toLowerCase()){
//            case "qwerty":
//                this.currKeys = qwerty;
//                break;
//
//            case "num":
//                this.currKeys = num;
//                break;
//        }
//    }

//    private void setKeys(){
//
//        switch(currKeyboard){
//            case -333:
//                currKeys = qwerty_special_chars;
//                break;
//
//            case -90:
//                currKeys = aeiou;
//                break;
//
//            case -99:
//                currKeys = qwerty;
//                break;
//
//            case -100:
//                currKeys = qwerty_1;
//                break;
//
//            case -101:
//                currKeys = qwerty_2;
//                break;
//
//            case -102:
//                currKeys = qwerty_3;
//                break;
//
//            case -103:
//                currKeys = qwerty_4;
//                break;
//
//            case -104:
//                currKeys = qwerty_5;
//                break;
//
//            case -123:
//                currKeys = numbers;
//                break;
//
//        }
//    }

    public void setKeyboard(String keyboardCode){
        switch (keyboardCode){
            case "qwerty_divided_view":
                currKeys = qwerty_divided_view ;

                break;
            case "qwerty_question":
                currKeys = qwerty_question ;

                break;
            case "qwerty_divided_view_dot":
                currKeys = qwerty_dot ;

                break;
            case "qwerty_divided_view_dot_dot":
                currKeys = qwerty_dot_dot ;

                break;
            case "qwerty_divided_view_qw":
                currKeys = qwerty_qw ;

                break;
            case "qwerty_divided_view_er":
                currKeys = qwerty_er ;

                break;
            case "qwerty_divided_view_ty":
                currKeys = qwerty_ty ;

                break;
            case "qwerty_divided_view_op":
                currKeys = qwerty_op ;

                break;
            case "qwerty_divided_view_as":
                currKeys = qwerty_as ;

                break;
            case "qwerty_divided_view_df":
                currKeys = qwerty_df  ;

                    break;
            case "qwerty_divided_view_gh":
                currKeys = qwerty_gh  ;

                break;
            case "qwerty_divided_view_jk":
                currKeys = qwerty_jk  ;

                break;
            case "qwerty_divided_view_ui":
                currKeys = qwerty_ui  ;

                break;
            case "qwerty_divided_view_xc":
                currKeys = qwerty_xc  ;

                break;
            case "qwerty_divided_view_vb":
                currKeys = qwerty_vb  ;

                break;
            case "qwerty_divided_view_nm":
                currKeys = qwerty_nm  ;

                break;


            case "qwerty":
                currKeys = qwerty;

                break;
            case "num":
                currKeys = num;

                break;
        }

//        Log.e(TAG, "setKeyboard keycodes >>" + keyboardCode);

//        setKeys();
    }

    public int toCode(String text){
        int code =0;

        switch (text.toLowerCase()){
            case "del":
                code = -5;
                break;

            case "caps":
                code = -1;
                break;

            case "space":
                code = 32;
                break;

            case "upiši":
                code = -200;
                break;

            default:
                code = (int) text.charAt(0);
                break;
        }
        return code;
    }

    public int toString(String text){
        int code =0;

        switch (text.toLowerCase()){
            case "del":
                code = -5;
                break;

            case "caps":
                code = -1;
                break;

            case "space":
                code = 32;
                break;

            case "upiši":
                code = -200;
                break;

            default:
                code = (int) text.charAt(0);
                break;
        }
        return code;
    }

    public String getCode(int row, int column){
        return this.currKeys[row][column];
    }

}
